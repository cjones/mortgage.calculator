# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import functools

import flask
from flask import jsonify
from flask import request
import flask_cors
from gevent import monkey
from gevent import wsgi

import m_exceptions


monkey.patch_all()
app = flask.Flask(__name__)

CURRENT_INTEREST_RATE = 0.025  # In percent


def _get_current_interest_rate():
    return CURRENT_INTEREST_RATE


def _set_current_interest_rate(rate):
    CURRENT_INTEREST_RATE = rate
    return CURRENT_INTEREST_RATE


def generate_response(func):
    @functools.wraps(func)
    def decorated(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
            return flask.jsonify(result)
        except AttributeError as ae:
            raise m_exceptions.BadRequest(str(ae))
    return decorated


@app.route('/')
def index():
    return 'Hello World'


@app.route('/payment-amount', methods=['GET'])
@generate_response
def payment_amount(*args, **kwargs):
    """ This calculates your payment amount based on several variables.

    Note: This function returns a rounded result to two decimal places.
    """
    args_valid = _validate_payment_args(request.args)
    if args_valid:
        result = _calculate_payment_amount(request.args)
        result_dict = {}
        payment_dict = {'payment_amount': _round_result(result)}
        result_dict = {'PaymentResponse': payment_dict}
        return result_dict
    else:
        raise m_exceptions.BadRequest("Invalid Args")


@app.route('/mortgage-amount', methods=['GET'])
@generate_response
def mortgage_amount(*args, **kwargs):
    """ This calculates your maximum mortgage amount based on several variables.

    Note: This function returns a rounded result to two decimal places.
    """
    opt_down_payment = request.args.get('down_payment', None)
    args_valid = _validate_mortgage_args(request.args, opt_down_payment)
    if args_valid:
        result = _calculate_mortgage_amount(request.args, opt_down_payment)
        result_dict = {}
        mortgage_dict = {'mortgage_amount': _round_result(result)}
        result_dict = {'MortgageResponse': mortgage_dict}
        return result_dict
    else:
        raise m_exceptions.BadRequest("Invalid Args")


@app.route('/interest-rate', methods=['PUT'])
@generate_response
def interest_rate(*args, **kwargs):
    args_valid = _validate_interest_args(request.args)
    if args_valid:
        current_rate = _get_current_interest_rate()
        result = _set_current_interest_rate(float(
                                            request.args['interest_rate']))
        result_dict = {}
        interest_dict = {'old_interest_rate': current_rate,
                         'new_interest_rate': result}
        result_dict = {'InterestResponse': interest_dict}
        return result_dict
    else:
        raise m_exceptions.BadRequest("Invalid Args")


@app.errorhandler(m_exceptions.BadRequest)
def handle_bad_request(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


def _validate_payment_args(args):
    c1 = _down_payment_ask_check(float(args['down_payment']),
                                 float(args['asking_price']))
    c2 = _payment_schedule_check(args['payment_schedule'])
    c3 = _amortization_check(float(args['amortization_period']))
    if (c1 and c2 and c3):
        return True
    return False


def _validate_mortgage_args(args, down_payment):
    if down_payment:
        c0 = _down_payment_check(down_payment)
        if not c0:
            return False
    c1 = _payment_schedule_check(args['payment_schedule'])
    c2 = _amortization_check(float(args['amortization_period']))
    if (c1 and c2):
        return True
    return False


def _validate_interest_args(args):
    if 0 <= float(args['interest_rate']):
        return True
    return False


def _amortization_check(value):
    if 5 <= value <= 25:
        return True
    return False


def _sanity_down_payment_check(down, price):
    # Reject nonsense input. We do not want to pay for something with negative
    # money nor pay more for something than it is worth.
    if price < 0 or down < 0:
        return False
    elif down > price:
        return False
    # Clearly if we pay for something outright, we are good.
    elif down == price:
        return True
    else:  # down < price  (fix logic with above)
        return True


def _is_mortgage_percent_valid(ratio):
    ret_val = False
    if ratio >= 0.05:
        ret_val = True
    return ret_val


def _is_insurance_required(ratio):
    ret_val = False
    if ratio < 0.20:
        ret_val = True
    return ret_val


def _is_insurance_available(down, price):
    ret_val = False
    if price - down <= 1000000:
        ret_val = True
    return ret_val


def _get_insurance_rate(ratio):
    if ratio < 0.10:
        return 0.0315
    elif ratio < 0.15:
        return 0.024
    elif ratio < 0.20:
        return 0.018
    else:
        return 0


def _down_payment_check(down_payment):
    # Reject nonsense input.
    ret_val = False
    if down_payment > 0:
        return True
    return ret_val


def _down_payment_ask_check(down_payment, price):
    minimum_bracket = 500000
    minimum_down = 25000
    over_min_pct = 0.10
    less_min_pct = 0.05
    if not _sanity_down_payment_check(down_payment, price):
        return False
    if price > minimum_bracket:
        minimum_down += over_min_pct * (price - minimum_bracket)
    else:
        minimum_down = less_min_pct * (price)

    if down_payment >= minimum_down:
        return True
    return False


def _payment_schedule_check(value):
    valid_values = ['Weekly', 'biweekly', 'monthly']
    if value in valid_values:
        return True
    return False


def _convert_schedule_number(value):
    if value == 'Weekly':
        return 52
    elif value == 'biweekly':
        return 26
    elif value == 'monthly':
        return 12
    else:
        # TODO(cjones): We should never get here as these values should have
        # been validated prior to getting here.
        # The safest thing to do would be to raise and Exception
        return 0


def _calculate_payment_amount(args):
    """Follow the formula given in the assignment.
    Payment formula: P = L[c(1 + c)^n]/[(1 + c)^n - 1]
    P = Payment
    L = Loan Principal
    c = Interest Rate

    Note: This function assumes that all the arguments have been validated.
    """
    num_payments_year = _convert_schedule_number(args['payment_schedule'])
    amortization_period = float(args['amortization_period'])
    loan = _calculate_loan_amount(float(args['asking_price']),
                                  float(args['down_payment']))
    one_plus_c_n, c_value = (
        _calculate_one_plus_c_to_the_n(num_payments_year,
                                       amortization_period)
    )
    numerator = loan * c_value * one_plus_c_n
    denominator = one_plus_c_n - 1
    result = numerator / denominator
    return result


def _calculate_loan_amount(price, down):
    if not _is_insurance_available(down, price):
        raise m_exceptions.BadRequest('Loan amount is too much. Insurance '
                                      'is unavailable')
    ratio = down / price
    insurance_req = _is_insurance_required(ratio)
    loan = price - down
    if insurance_req:
        insurance_rate = _get_insurance_rate(ratio)
        loan = loan * (1 + insurance_rate)
    return loan


def _calculate_one_plus_c_to_the_n(num_payments_per_year,
                                   amortization_period):
    c_value = CURRENT_INTEREST_RATE / num_payments_per_year
    one_plus_c_n = pow((1 + c_value),
                       (amortization_period * num_payments_per_year))
    return one_plus_c_n, c_value


def _calculate_mortgage_amount(args, down_payment):
    """Follow the formula given in the assignment, but solve for L
    Payment formula: L = P[(1 + c)^n - 1]/[c(1 + c)^n]
    P = Payment
    L = Loan Principal
    c = Interest Rate

    Note: This function assumes that all the arguments have been validated.
    """
    payment_amount = float(args['payment_amount'])
    num_payments_year = _convert_schedule_number(args['payment_schedule'])
    amortization_period = float(args['amortization_period'])
    one_plus_c_n, c_value = (
        _calculate_one_plus_c_to_the_n(num_payments_year,
                                       amortization_period)
    )
    numerator = payment_amount * (one_plus_c_n - 1)
    denominator = c_value * one_plus_c_n
    loan = numerator / denominator
    if down_payment:
        loan = loan + down_payment
    return loan


def _round_result(result):
    round_result = "{0:.2f}".format(result)
    return round_result


if __name__ == '__main__':
    app.debug = True
    print('Mortgage Calculator server started in debug mode: {}'.format(
        app.debug)
    )
    flask_cors.CORS(app)
    server = wsgi.WSGIServer(('', 80), app)
    server.serve_forever()
