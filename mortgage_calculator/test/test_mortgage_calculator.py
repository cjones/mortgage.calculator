# vim: tabstop=4 shiftwidth=4 softtabstop=4
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import unittest

from mortgage_calculator import mortgage_calculator_server as mcs


class TestMortgageCalculator(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def _assert_equal(self, result, expected):
        assert result == expected, 'result: {} is not == b: {}'.format(
            result, expected
        )

    def test_get_interest_rate(self):
        result = mcs._get_current_interest_rate()
        self._assert_equal(0.025, result)

    def test_set_interest_rate(self):
        result = mcs._set_current_interest_rate(0.04)
        self._assert_equal(0.04, result)

    @unittest.skip('Need to implement')
    def test_validate_payment_args(self):
        pass

    @unittest.skip('Need to implement')
    def test_validate_mortgage_args(self):
        pass

    def test_validate_interest_args(self):
        minimum = 0
        args = {
            'interest_rate': minimum
        }
        result = mcs._validate_interest_args(args)
        self._assert_equal(result, True)
        args['interest_rate'] = minimum - 1
        result = mcs._validate_interest_args(args)
        self._assert_equal(result, False)
        args['interest_rate'] = minimum + 1
        result = mcs._validate_interest_args(args)
        self._assert_equal(result, True)

    def test_amortization_check(self):
        minimum = 5
        maximum = 25
        # Check minimums
        result = mcs._amortization_check(minimum)
        self._assert_equal(result, True)
        result = mcs._amortization_check(minimum - 1)
        self._assert_equal(result, False)
        result = mcs._amortization_check(minimum + 1)
        self._assert_equal(result, True)
        # Check maximums
        result = mcs._amortization_check(maximum)
        self._assert_equal(result, True)
        result = mcs._amortization_check(maximum - 1)
        self._assert_equal(result, True)
        result = mcs._amortization_check(maximum + 1)
        self._assert_equal(result, False)

    def test_is_mortgage_percent_valid(self):
        # Check minimums
        ratio = 0.0499
        result = mcs._is_mortgage_percent_valid(ratio)
        self._assert_equal(result, False)
        result = mcs._is_mortgage_percent_valid(0.05)
        self._assert_equal(result, True)
        result = mcs._is_mortgage_percent_valid(0.0501)
        self._assert_equal(result, True)

    def test_insurance_checks(self):
        result = mcs._is_insurance_available(0, 1000000)
        self._assert_equal(result, True)
        result = mcs._is_insurance_available(0, 1000001)
        self._assert_equal(result, False)
        result = mcs._is_insurance_available(1, 1000000)
        self._assert_equal(result, True)
        result = mcs._is_insurance_available(1, 1000001)
        self._assert_equal(result, True)
        result = mcs._is_insurance_required(0.0)
        self._assert_equal(result, True)
        result = mcs._is_insurance_required(0.19999)
        self._assert_equal(result, True)
        result = mcs._is_insurance_required(0.20)
        self._assert_equal(result, False)
        result = mcs._is_insurance_required(0.20001)
        self._assert_equal(result, False)

    def test_get_insurance_rate(self):
        result = mcs._get_insurance_rate(0.00)
        self._assert_equal(result, 0.0315)
        result = mcs._get_insurance_rate(0.09)
        self._assert_equal(result, 0.0315)
        result = mcs._get_insurance_rate(0.10)
        self._assert_equal(result, 0.024)
        result = mcs._get_insurance_rate(0.14999)
        self._assert_equal(result, 0.024)
        result = mcs._get_insurance_rate(0.15)
        self._assert_equal(result, 0.018)
        result = mcs._get_insurance_rate(0.19999)
        self._assert_equal(result, 0.018)
        result = mcs._get_insurance_rate(0.20)
        self._assert_equal(result, 0)

    def test_down_payment_ask_check(self):
        minimum_down = 0  # zero dollars
        minimum_price = 0  # zero dollars
        # Check minimums
        result = mcs._down_payment_ask_check(minimum_down, minimum_price)
        self._assert_equal(result, True)
        result = mcs._down_payment_ask_check(minimum_down - 1, minimum_price)
        self._assert_equal(result, False)
        result = mcs._down_payment_ask_check(minimum_down - 1,
                                             minimum_price - 1)
        self._assert_equal(result, False)
        result = mcs._down_payment_ask_check(minimum_down, minimum_price - 1)
        self._assert_equal(result, False)
        # Check over minimums
        result = mcs._down_payment_ask_check(minimum_down + 1, minimum_price)
        self._assert_equal(result, False)
        result = mcs._down_payment_ask_check(minimum_down + 1,
                                             minimum_price + 1)
        self._assert_equal(result, True)
        result = mcs._down_payment_ask_check(minimum_down, minimum_price + 1)
        self._assert_equal(result, False)
        # Check larger values:
        result = mcs._down_payment_ask_check(minimum_down + 1500000,
                                             minimum_price)
        self._assert_equal(result, False)
        result = mcs._down_payment_ask_check(minimum_down + 1500000,
                                             minimum_price + 1)
        self._assert_equal(result, False)
        # Check thresholds:
        result = mcs._down_payment_ask_check(minimum_down + 25000,
                                             minimum_price + 500000)
        self._assert_equal(result, True)
        result = mcs._down_payment_ask_check(minimum_down + 24999,
                                             minimum_price + 500000)
        self._assert_equal(result, False)
        result = mcs._down_payment_ask_check(minimum_down + 25001,
                                             minimum_price + 500000)
        self._assert_equal(result, True)
        result = mcs._down_payment_ask_check(minimum_down + 25001,
                                             minimum_price + 500001)
        self._assert_equal(result, True)
        result = mcs._down_payment_ask_check(minimum_down + 25000,
                                             minimum_price + 1000000)
        self._assert_equal(result, False)
        result = mcs._down_payment_ask_check(minimum_down + 75000,
                                             minimum_price + 1000000)
        self._assert_equal(result, True)
        result = mcs._down_payment_ask_check(minimum_down + 75001,
                                             minimum_price + 1000000)
        self._assert_equal(result, True)
        result = mcs._down_payment_ask_check(minimum_down + 74999,
                                             minimum_price + 1000000)
        self._assert_equal(result, False)

    def test_payment_schedule_check(self):
        valid_values = ['Weekly', 'biweekly', 'monthly']
        for value in valid_values:
            assert mcs._payment_schedule_check(value)

        invalid_values = ['one', 'two', 3]
        for value in invalid_values:
            assert not mcs._payment_schedule_check(
                value)

    def test_convert_schedule_number(self):
        self._assert_equal(mcs._convert_schedule_number('Weekly'), 52)
        self._assert_equal(mcs._convert_schedule_number('biweekly'), 26)
        self._assert_equal(mcs._convert_schedule_number('monthly'), 12)
        self._assert_equal(mcs._convert_schedule_number('foo'), 0)

    def test_calculate_payment_amount_no_interest(self):
        """Test core calculation.

        Using this site for some verification:
        http://www.calculator.net/payment-calculator.html

        Since the function under test assumes all arguments have been
        previously validated, we will not be passing in any bogus values.
        """
        args = {
            'payment_schedule': 'monthly',
            'amortization_period': 20,
            'asking_price': 300000,
            'down_payment': 100000,
        }
        result = mcs._calculate_payment_amount(args)
        self._assert_equal(mcs._round_result(result), "1059.81")

    def test_calculate_mortgage_amounts(self):
        """Test core calculation.

        If we can assume the above "test_calculate_payment_amount_no_interest"
        verifies things correctly, we can then essentially test the inverse and
        demonstrate that this correctly computes the maximum mortgage. We may
        have to assume some room for rounding error.

        Since the function under test assumes all arguments have been
        previously validated, we will not be passing in any bogus values.
        """
        args1 = {
            'payment_schedule': 'monthly',
            'amortization_period': 20,
            'asking_price': 300000,
            'down_payment': 100000,
        }
        result = mcs._calculate_payment_amount(args1)
        # This should give us a result similar to rounded result above.
        # Let's plug this into the other function and see if we get something
        # similar:
        args2 = {
            'payment_amount': result,
            'payment_schedule': 'monthly',
            'amortization_period': 20,
            'down_payment': 100000,
        }
        result = mcs._calculate_mortgage_amount(args2,
                                                float(args2['down_payment']))
        round_asking = mcs._round_result(args1['asking_price'])
        self._assert_equal(mcs._round_result(result), round_asking)

        # Now test without a downpayment
        args3 = {
            'payment_amount': args2['payment_amount'],
            'payment_schedule': 'monthly',
            'amortization_period': 20,
            'down_payment': 0,
        }
        result = mcs._calculate_mortgage_amount(args3,
                                                float(args3['down_payment']))
        round_asking = mcs._round_result(args1['asking_price'])
        down_payment = mcs._round_result(args1['down_payment'])
        net_mortgage = float(round_asking) - float(down_payment)
        self._assert_equal(mcs._round_result(result),
                           mcs._round_result(net_mortgage))

    def test_calculate_payment_amount_with_insurance(self):
        """Test core calculation - With < 20% down.

        Since the function under test assumes all arguments have been
        previously validated, we will not be passing in any bogus values.

        We also know from http://www.calculator.net/payment-calculator.html
        that a 400,000 mortgage at 25 year amortization at 2.5% will have a
        monthly cost of 1,826.77

        We can roughly predict that the ~$400,000 mortgage will cost another
        1.8 percent. So we can assume that this will be an extra $7200 over
        the life of the mortgage.

        These two values should line up.
        """

        # First assume no insurance, but calculate the estimated amount
        args1 = {
            'payment_schedule': 'monthly',
            'amortization_period': 25,
            'asking_price': 607201,
            'down_payment': 200000,
        }
        result1 = mcs._calculate_payment_amount(args1)
        expected_result1 = "1826.77"
        self._assert_equal(mcs._round_result(result1), expected_result1)
        args2 = {
            'payment_schedule': 'monthly',
            'amortization_period': 25,
            'asking_price': 500000,
            'down_payment':  99999,
        }
        result2 = mcs._calculate_payment_amount(args2)
        self._assert_equal(mcs._round_result(result2), expected_result1)

    @unittest.skip('Need to implement')
    def test_calculate_payment_amount_with_insurance_15(self):
        """Test core calculation - With < 15% down.
        """
        pass

    @unittest.skip('Need to implement')
    def test_calculate_payment_amount_with_insurance_10(self):
        """Test core calculation - With < 10% down.
        """
        pass

    @unittest.skip('Need to implement')
    def test_calculate_payment_amount_with_insurance_5(self):
        """Test core calculation - With < 5% down.
        """
        pass
