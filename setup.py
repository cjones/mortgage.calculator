from setuptools import setup

setup(
    name='mortgage_calculator',
    version='0.1',
    install_requires=[],
    packages=['mortgage_calculator'],
    package_data={'mortgage_calculator': ['*.py']},
    entry_points='''
        [console_scripts]
        mortgage_calculator=mortgage_calculator.shell:cli
    ''',
    test_suite='nose.collector',
    tests_require=['nose'],
)
