Things that need to be improved
==========

- Perhaps stop running the server in debug mode.
- Add logging and logging levels debug/error/etc.
- Add in port configuration so that this program does not need to be run by
  root.
- Add in better testing. Should use assertTrue/False().
- Add in better documentation / docstrings to add in input/output assumptions.
- Add in skip tests where required if test stubs are in place, but not implemented.
- Add in tests for additional payment schedules Weekly/Biweekly.
- Add in tests that exercise the change in interest rate and make sure it computes properly.
