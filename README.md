
API and problem description
==========
Create a mortgage calculator API using any language/tech stack.
The API should accept and return JSON.

A definition has been defined in swagger.io and committed for your convenience
in this repository at doc/source/api.yml

**GET** */payment-amount*

Get the recurring payment amount of a mortgage

    Params:
        Asking Price
        Down Payment [1]*
        Payment schedule [3]*
        Amortization Period [2]*

    Return:
        Payment amount per scheduled payment


**GET** */mortgage-amount*

Get the maximum mortgage amount

    Params:
        payment amount
        Down Payment(optional) [4]*
        Payment schedule [3]*
        Amortization Period [2]*

    Return:
        Maximum Mortgage that can be taken out

**PATCH** */interest-rate*

Change the interest rate used by the application

    Params:
        Interest Rate


    Return:
        message indicating the old and new interest rate


**Notes:**

* [1]* Must be at least 5% of first $500k plus 10% of any amount above $500k
  (So $50k on a $750k mortgage)
* [2]* Min 5 years, max 25 years
* [3]* Weekly, biweekly, monthly
* [4]* If included its value should be added to the maximum mortgage returned

* Mortgage interest rate 2.5% per year

* Mortgage insurance is required on all mortgages with less than 20% down.
* Insurance must be calculated and added to the mortgage principal.
* Mortgage insurance is not available for mortgages > $1 million.

**Mortgage insurance rates are as follows:**


|Downpayment  |Insurance Cost|
|-------------|:------------:|
|5-9.99%      | 3.15%        |
|10-14.99%    | 2.4%         |
|15%-19.99%   | 1.8%         |
|20%+         | N/A          |



Payment formula: P = L[c(1 + c)^n]/[(1 + c)^n - 1]


|   | |                |
| --|-|:--------------:|
| P |=| Payment        |
| L |=| Loan Principal |
| c |=| Interest Rate  |

Note: c is the interest rate divided by the number of payments. So, if payments
are monthly, c is the principal interest rate divided by 12. If payments are
weekly, then c is the principal interest rate divided by 52.

How to install this program
==========

You will need virtualenv and pip pre installed on your machine.

Following the rest of the instructions will install this in a pip

virtual environment and should not pollute your machine in any other way.

If you choose to run on a port other than 80, you can choose another port above

the applicable OS range where `sudo` is not required.

* virtualenv .venv
* source .venv/bin/activate
* pip install -r requirements.txt
* sudo python mortgage_calculator/mortgage_calculator_server.py

**Note: sudo is only required due to running the server on port 80.**
**In a real world application, this would be run by a proper web service such
  as apache and could be restarted by `sudo service apache restart`**

How to sanity check installation
==========

To determine if the server is up and alive, please make a GET request to the
following:

http://localhost`<:port>`

Should perform a GET and return "Hello World"


How to use this program
==========

Once the service has been started in a virtual environment or otherwise, all
API functionality should be available as described above. Please make your
GET and PATCH requests using your favourite utility.

You can either:

* Load the api.yml into, http://editor.swagger.io, download the full fledged
application.
* Use Postman https://www.getpostman.com. There is a fullfledged
application or plugin for most modern browsers.
* Use your browser directly and type the desired URL into the URL bar to
perform the GET operations.

How to test this program
==========

The curent test environment uses tox which runs the tests in a virtual
environment.

For PEP8 tests, please enter at the command line:
* tox -e pep8

For PY27 tests, please enter at the command line:
* tox -e py27

**Note: a -v flag may be passed in to tox for more verbose output for
  debugging.**

Known limitations
==========

The server does not currently use a backend database/file to store the current
interest rate.
This is currently stored in RAM and is an intentional over simplification.
Restarting the service will result in the interest rate being set back to the
default value.

Brief description of the program's logic
==========

Flask is used to set up the basic application and route the URL routes to the
respective functions with either GET/PATCH as requested in the assignment.
This is done via the @app.route() decoration.

After the inputs are validated, any calculations are made, and a response is
then hand packed into a dictionary which is then used by the @generate_response
decorator and returns the flask.jsonify(result).

General helper functions are denoted with an underscore prefix and are not
publicly consumable. Some assumptions are made about the inputs when these are
used such as values are non-negative, values are no longer strings, but are
numbers, etc. These hide some of the implementation details, but the
assumptions should be noted.

The bulk of the computation is done in the following two functions:
def _calculate_payment_amount(args):
def _calculate_mortgage_amount(args):

def _set_current_interest_rate(): currently just writes a global variable. This
is an over simplification for the purposes of this assignment and can easily
be re-written to write this value to a database/file/remote API
def _get_current_interest_rate(): currently just reads a global variable. This
is an over simplification for the purposes of this assignment and can easily
be re-written to read this value to a database/file/remote API
